import React, {Component} from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';
import { Text, Icon, TopNavigationAction, TopNavigation, BottomNavigation, Button } from '@ui-kitten/components';
import { mapping, light as lightTheme } from "@eva-design/eva";
import {NavigationMenu} from './NavigationMenu';
import Header from "./Header";
import TodoList from "./TodoList";
import Footer from "./Footer";

const MenuIcon = (style) => (
    <Icon {...style} name='menu-outline' />
);

export class TaskPage extends React.Component {

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#cfa57e'}}>
                <SafeAreaView style={{flex: 1}}>
                    <View style={{ flex: 1, backgroundColor: '#734728' }}>
                        <View style={{flex: 1}}>
                            <Header/>
                        </View>
                        <View style={{flex: 8}}>
                            <TodoList/>
                        </View>
                    </View>
                </SafeAreaView>
            </View>
        );
    };
}

export default TaskPage;