import React, {Component} from 'react';
import { StyleSheet, View, SafeAreaView } from 'react-native';
import { Text, Icon, ListItem, List, TopNavigation, TopNavigationAction, Divider } from '@ui-kitten/components';
import { mapping, light as lightTheme } from "@eva-design/eva";
import NavigationMenu from './NavigationMenu';
import Header from "./Header";

const BackIcon = (style) => (
    <Icon {...style} name='arrow-back' />
);

export class Calendar extends React.Component {

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#cfa57e'}}>
                <View style={{flex: 1}}>
                    <Header/>
                </View>
                <View style={{flex: 8, backgroundColor: '#734728', padding: 15}}>
                    <View style={{backgroundColor: '#cfa57e', padding: 15, borderRadius: 10}}>
                        <Text category='h3'>CALENDAR</Text>
                    </View>
                    <View style={{flex: 1, paddingTop: 20, paddingBottom: 20}}>
                        <View style={{flex: 1, backgroundColor: 'white', padding: 15, borderRadius: 5}}></View>
                    </View>
                </View>
            </SafeAreaView>
        );
    };
}

export default Calendar;