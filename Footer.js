import React from 'react';
import { StyleSheet, View, TextInput, SafeAreaView } from 'react-native';
import { Layout, Text, Button } from '@ui-kitten/components';


export default function Footer() {
    return (
        <View style ={{flex: 1, justifyContent: 'center', backgroundColor: '#cfa57e', width:"100%"}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <Text category="h6">TASKS</Text>
                <Text category="h6">CALENDAR</Text>
                <Text category="h6">SHOP</Text>
            </View>
        </View>
    )
}