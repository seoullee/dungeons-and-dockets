import React from 'react';
import { StyleSheet, View, TextInput, SafeAreaView } from 'react-native';
import { Layout, Text, Button } from '@ui-kitten/components';

export default function Header() {
    return (
            <View style ={{flex: 1,  backgroundColor: '#cfa57e', width:"100%"}}>
                <SafeAreaView style = {{ alignItems: 'center'}}>
                    <Text category="h2">Dungeons & Dockets</Text>
                </SafeAreaView>
            </View>
    )
}