import React, {Component} from 'react';
import { StyleSheet, View, SafeAreaView, FlatList } from 'react-native';
import {Text, Icon, ListItem, List, TopNavigation, TopNavigationAction, Divider, Button} from '@ui-kitten/components';
import { mapping, light as lightTheme } from "@eva-design/eva";
import Header from "./Header";

export class ShopPage extends React.Component {

   render() {
        return (
                <SafeAreaView style={{flex: 1, justifyContent: 'center', backgroundColor: '#cfa57e'}}>
                    <View style={{flex: 1}}>
                        <Header/>
                    </View>
                    <View style={{flex: 8, backgroundColor: '#734728', padding: 15}}>
                        <View style={{}}>
                            <View style={{padding: 15, backgroundColor: '#cfa57e', borderRadius: 10}}>
                                <Text category="h3">SHOP</Text>
                            </View>
                                <Text category="h5">Axes</Text>
                            <View style={{paddingBottom: 10, paddingTop: 10}}>
                                <Button status='basic'>Diamond</Button>
                            </View>
                            <View style={{backgroundColor: 'white', paddingLeft: 10, paddingTop: 20, paddingBottom: 20, borderRadius: 4}}>
                                <Text>Diamond</Text>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
        );
    };
}

export default ShopPage;

const styles = StyleSheet.create({
    shopItem: {
        paddingTop: 10,
        paddingBottom: 10,
    },
});