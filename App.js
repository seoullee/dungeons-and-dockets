import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { StyleSheet, View, TextInput, FlatList, SafeAreaView } from 'react-native';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { mapping, light as lightTheme } from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Calendar from "./Calendar"
import TaskPage from "./TaskPage";
import ShopPage from "./ShopPage";

const Tab = createBottomTabNavigator();

class App extends React.Component {

    render() {
        return (
            <React.Fragment>
                <IconRegistry icons={EvaIconsPack}/>
                <ApplicationProvider mapping={mapping} theme={lightTheme}>
                    <NavigationContainer>
                        <Tab.Navigator initialRouteName = "Tasks">
                            <Tab.Screen name="TASKS" component={TaskPage} />
                            <Tab.Screen name="CALENDAR" component={Calendar} />
                            <Tab.Screen name="SHOP" component={ShopPage}/>
                        </Tab.Navigator>
                    </NavigationContainer>
                </ApplicationProvider>
            </React.Fragment>
        );
    }
}

export default App;