import React, {Component} from 'react';
import { StyleSheet, Button, StatusBar, FlatList, AppRegistry, View, AsyncStorage, TextInput, Keyboard, Platform, Image } from 'react-native';
// import {AsyncStorage} from 'react-native-community/async-storage';
import { Text, Input } from '@ui-kitten/components';
import { mapping, light as lightTheme } from '@eva-design/eva';
// import Image from "react-native-web/src/exports/Image";


const viewPadding = 10;
const points = 10;

export class UserStats extends Component {
    render() {
        return(
            <View style ={{flex: 1, justifyContent: 'center', backgroundColor: '#ffe4c4', width:"100%"}}>
                {/*<View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 20, paddingRight: 20}}>*/}
                {/*    <View style={{alignSelf: 'flex-start'}}>*/}
                {/*        <Image*/}
                {/*            source={{uri: 'https://pbs.twimg.com/profile_images/378800000478240318/f2fc7a4b01ced45f4289242c51fa05c7.png'}}*/}
                {/*            style={{width: 75, height: 75}}*/}
                {/*        />*/}
                {/*        <Text>AVATAR</Text>*/}
                {/*    </View>*/}
                {/*    <View style={{alignSelf: 'flex-end'}}>*/}
                {/*        <Text>POINTS: </Text>*/}
                {/*        <Text>LEVEL:</Text>*/}
                {/*    </View>*/}
                {/*</View>*/}
            </View>
        );
    }
};

export default class TodoList extends Component {
    state = {
        tasks: [],
        text: "",
        points: 0
    };

    changeTextHandler = text => {
        this.setState({ text: text });
    };

    addTask = () => {
        let notEmpty = this.state.text.trim().length > 0;

        if (notEmpty) {
            this.setState(
                prevState => {
                    let { tasks, text } = prevState;
                    return {
                        tasks: tasks.concat({ key: tasks.length, text: text }),
                        text: ""
                    };
                },
                () => Tasks.save(this.state.tasks)
            );
        }
    };

    deleteTask = i => {
        this.setState(
            prevState => {
                let tasks = prevState.tasks.slice();

                tasks.splice(i, 1);

                return { tasks: tasks };
            },
            () => Tasks.save(this.state.tasks)
        );
    };

    addPoints = () => {
        this.setState({points: this.state.points + 5});
    };

    render() {
        return (
            <View
                // style={{ paddingBottom: this.state.viewPadding }}
            >
                <View style={{justifyContent: 'center', backgroundColor: '#cfa57e'}}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: 30, paddingRight: 30}}>
                        <View>
                            <Image
                                source={{uri: 'https://pbs.twimg.com/profile_images/378800000478240318/f2fc7a4b01ced45f4289242c51fa05c7.png'}}
                                style={{width: 75, height: 75}}
                            />
                            <Text category="s2">USERNAME</Text>
                        </View>
                        <View>
                            <Text category="h6">POINTS: {this.state.points}</Text>
                            <Text category="h6">LEVEL:</Text>
                        </View>
                    </View>
                </View>
                <View style={{paddingRight: 15, paddingLeft: 15}}>
                <View style={{paddingTop: 15, paddingBottom: 15}}>
                    <Input
                        onChangeText={this.changeTextHandler}
                        onSubmitEditing={this.addTask}
                        value={this.state.text}
                        placeholder="Add Tasks"
                        returnKeyType="done"
                        returnKeyLabel="done"
                    />
                </View>
                <View>
                <FlatList
                    data={this.state.tasks}
                    renderItem={({ item, index }) =>
                        <View>
                            <View style={{paddingBottom: 5}}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    paddingLeft: 20,
                                    paddingRight: 20,
                                    paddingBottom: 15,
                                    backgroundColor: 'white',
                                    borderRadius: 5
                                }}>
                                <Text category="s1">
                                    {item.text}
                                </Text>
                                <Button title="X" onPress={() => {this.deleteTask(index); this.addPoints()}} /></View>
                            </View>
                            <View />
                        </View>}
                /></View>
            </View>
            </View>
        );
    }
}

let Tasks = {
    convertToArrayOfObject(tasks, callback) {
        return callback(
            tasks ? tasks.split("||").map((task, i) => ({ key: i, text: task })) : []
        );
    },
    convertToStringWithSeparators(tasks) {
        return tasks.map(task => task.text).join("||");
    },
    all(callback) {
        return AsyncStorage.getItem("TASKS", (err, tasks) =>
            this.convertToArrayOfObject(tasks, callback)
        );
    },
    save(tasks) {
        AsyncStorage.setItem("TASKS", this.convertToStringWithSeparators(tasks));
    }
};

AppRegistry.registerComponent("TodoList", () => TodoList);